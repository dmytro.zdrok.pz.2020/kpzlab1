﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DmytroZdrok
{
    public class DmytroZdrokAlgorithm : IRobotAlgorithm
    {
        List<Position> positionsOfMoreChargers = new List<Position>();
        bool wasLearned = false;
        int round = 0;
        public string Author => "Zdrok Dmytro";

        public DmytroZdrokAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            round++;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var robotPos = robots[robotToMoveIndex].Position;

            var nearestStation = DistanceHelper.FindNearestFreeStation(robots[robotToMoveIndex], map, robots);

            var distanceToStation = DistanceHelper.FindDistance(nearestStation, robotPos);

            //if (robots.Where(x => DistanceHelper.FindDistance(x.Position, robotPos) <= robots[robotToMoveIndex].Energy + 50
            //&& x.Energy >= robots[robotToMoveIndex].Energy * 25 && x.OwnerName != "Zdrok Dmytro").ToList().Count >= 1 && robots[robotToMoveIndex].Energy >= 52)
            //{
            //    return new MoveCommand() { NewPosition = robots.Where(x => DistanceHelper.FindDistance(x.Position, robotPos) <= robots[robotToMoveIndex].Energy + 50
            //&& x.Energy >= robots[robotToMoveIndex].Energy * 25 && x.OwnerName != "Zdrok Dmytro").ToList().First().Position };
            //}

            if (round>=40)
            {
                return new CollectEnergyCommand();
            }

            if (!wasLearned)
            {
                FindPoints(map);
            }

            if (robots[robotToMoveIndex].Energy >= 300 && RobotCount(robots) < 100 && distanceToStation <= 10)
            {
                return new CreateNewRobotCommand();
            }

            if (positionsOfMoreChargers.Contains(robots[robotToMoveIndex].Position) && SumOfEnergy(map, robotPos) > 0)
            {
                return new CollectEnergyCommand();
            }

            var allBestPos = positionsOfMoreChargers.Where(x =>
            DistanceHelper.FindDistance(robots[robotToMoveIndex].Position, x) <= robots[robotToMoveIndex].Energy &&
            DistanceHelper.IsCellFree(x, robots[robotToMoveIndex], robots)
            && IsRobotsAroundPosition2(robots, robots[robotToMoveIndex], x))
                .OrderByDescending(x => CountOfChargers(x, map)).ToList();

            if (allBestPos.Count!=0)
            {
                var bestPos = allBestPos.First();
                return new MoveCommand() { NewPosition = bestPos };
            }
            
            if(distanceToStation<=robots[robotToMoveIndex].Energy && !IsRobotOnStation(robots[robotToMoveIndex], map) && RobotCountInRadius("Zdrok Dmytro", robots, robotPos, 1) == 0)
            {
                return new MoveCommand() { NewPosition = nearestStation };
            }

            if (IsRobotOnStation(robots[robotToMoveIndex], map) && RobotCountInRadius("Zdrok Dmytro", robots, robotPos, 2) == 1 && map.Stations.Where(x => x.Position == robotPos).ToList().First().Energy > 0)
            {
                return new CollectEnergyCommand();
            }


            var dX = Math.Sign(nearestStation.X - robotPos.X);

            var dY = Math.Sign(nearestStation.Y - robotPos.Y);

            return new MoveCommand() { NewPosition = new Position(robotPos.X + dX, robotPos.Y + dY) };
        }

        public static bool IsRobotOnStation(Robot.Common.Robot robot, Map map)
        {
            foreach (var item in map.Stations)
            {
                if (robot.Position==item.Position)
                {
                    return true;
                }
            }
            return false;
        }

        public static int RobotCountInRadius(string ownerName, IList<Robot.Common.Robot> robots, Position pos, int R)
        {
            return robots.Where(x => x.OwnerName == ownerName && DistanceHelper.FindDistance(x.Position, pos) <= (int)Math.Pow(2, R)).ToList().Count();
        }

        public static bool IsRobotsAroundPosition1(IList<Robot.Common.Robot> robots, Robot.Common.Robot robot, Position pos)
        {
            foreach (var item in robots)
            {
                if (item.OwnerName == "Zdrok Dmytro" && DistanceHelper.FindDistance(item.Position, pos) <= 2 && item != robot)
                {
                    return false;
                }
            }
            return true;
        }
        bool IsRobotsAroundPosition2(IList<Robot.Common.Robot> robots, Robot.Common.Robot robot, Position pos)
        {
            foreach (var item in robots)
            {
                if (item.OwnerName == "Zdrok Dmytro" && DistanceHelper.FindDistance(item.Position, pos) <= 13 && item != robot)
                {
                    return false;
                }
            }
            return true;
        }

        int RobotCount(IList<Robot.Common.Robot> robots)
        {
            return robots.Where(x => x.OwnerName == "Zdrok Dmytro").ToList().Count;
        }

        void FindPoints(Map map)
        {
            for (int i = 0; i < 99; i++)
            {
                for (int j = 0; j < 99; j++)
                {
                    var p = new Position(i, j);
                    if (CountOfChargers(p, map) >= 2)
                    {
                        positionsOfMoreChargers.Add(p);
                    }
                }
            }
            wasLearned = true;
        }

        public static int CountOfChargers(Position position, Map map)
        {
            return map.Stations.Where(x => DistanceHelper.FindDistance(position, x.Position) <= 2).ToList().Count();
        }

        public static int SumOfEnergy(Map map, Position pos)
        {
            List<EnergyStation> energyStations = map.Stations.Where(x => DistanceHelper.FindDistance(x.Position, pos) <= 2).ToList();
            int sum = 0;
            foreach (var st in energyStations)
            {
                sum += st.Energy;
            }
            return sum;
        }
    }
}
